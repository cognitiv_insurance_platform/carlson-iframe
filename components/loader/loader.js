import Link from "next/link";
import styles from "./loader.module.css";

export default function loader() {
  return <div className={styles.loaderspin}></div>;
}
