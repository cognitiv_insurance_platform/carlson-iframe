import Head from 'next/head';
import Image from 'next/image';
import styles from '../styles/Home.module.css';
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import Loader from "../components/loader/loader";

export default function Home() {
  const router = useRouter();
  const [companyName, setCompanyName] = useState("");
  const [companySearchResults, setCompanySearchResult] = useState([]);


  const [companyAddress, setCompanyAddress] = useState("");
  const [companyActivities, setCompanyActivities] = useState("");

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");


  

  const changeCompanyName = (e) => {
    setCompanyName(e.target.value);
    if(e.target.value.length> 2){
      companySearch(e.target.value);
    }
  }
  const companySearch = async (searchText) => {
    // console.log(searchText);
    document.getElementById("companyInput").className =
      styles.form__input2Loading;
    setCompanyResult({});
    if (searchText.length > 2) {
      const response = await fetch(`${process.env.NEXT_PUBLIC_COMPANY_SEARCH}`,
        {
          method: "POST",
          body: JSON.stringify({ searchText }),
          headers: {
            "Content-Type": "application/json",
          },
        }
      ).then((res) => res.json());
      // console.log(response);
      //set search options
      await setCompanySearchResult(response);
    } else {
      //remove search options
      setCompanyResult({});
      setCompanySearchResult([]);
    }
    document.getElementById("companyInput").className = styles.form__input2;
  };
  const [companyResult, setCompanyResult] = useState({});
  const setCompany = async (value) => {
    // console.log(value.name);
    setCompanyResult(value);
    // setCompanyName()
    document.getElementById("companyInput").value = value.name;
    await setCompanySearchResult([]);
    await getAddressDetail(value);
  };

  const getAddressDetail = async (company) => {
    console.log(company);
    // const accessToken = await getAccessTokenSilently({
    //   audience: process.env["NEXT_PUBLIC_AUTH0_AUDIENCE"],
    // });
    // const company_register = "https://gc84ebamse.execute-api.ap-southeast-2.amazonaws.com/test/company-details";
    const response = await fetch(`${process.env["NEXT_PUBLIC_COMPANY_REGISTER"]}/${company.nzbn}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        // Authorization: `Bearer ${accessToken}`,
      },
    }).then((r) => r.json());
    console.log(response);
    // setFolioDetails({
    //   name: company.name,
    //   address: response.address.address1+', '+response.address.address2+', '+response.address.address3,
    //   bisActivities: folioDetails.bisActivities,
    // });
    let address = response.address.address1;
    if(!!response.address.address2){
      address += ", "+response.address.address2;
    }
    if(!!response.address.address3){
      address += ", "+response.address.address3;
    }
    setCompanyAddress(address);
    document.getElementById("companyAddress").value = address;
  }
  

  const [emailValidation, setEV] = useState(false);

  const checkEmail = (value) => {
    setEmail(value);
    let re = new RegExp(
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    let emailV = re.exec(value);
    let emailValid = !!emailV;
    setEV(emailValid);
  };

  const [passwordValidation, setPV] = useState({
    lowerCase: false,
    upperCase: false,
    special: false,
    numerical: false,
    eightChars: false,
  });

  const passwordCheck = (value) => {
    setPassword(value);
    let re = new RegExp(/(?=.*[a-z])/gm);
    let lowerCase = re.exec(value);
    re = new RegExp(/(?=.*[A-Z])/gm);
    let upperCase = re.exec(value);
    re = new RegExp(/(?=.*[!@#$&*])/gm);
    let special = re.exec(value);
    re = new RegExp(/(?=.*[0-9])/gm);
    let numerical = re.exec(value);
    re = new RegExp(/.{8,}$/gm);
    let eightChars = re.exec(value);

    let lowerCaseV = !!lowerCase; //!! is a double negative because return type is null for false and object for true
    // showValidation("lowerCaseCheckmark" ,state.passwordValidation.lowerCase);
    let upperCaseV = !!upperCase;
    // showValidation("upperCaseCheckmark" ,state.passwordValidation.upperCase);
    let specialV = !!special;
    // showValidation("specialCheckmark" ,state.passwordValidation.special);
    let numericalV = !!numerical;
    // showValidation("numericCheckmark" ,state.passwordValidation.numerical);
    let eightCharsV = !!eightChars;
    // showValidation("eightCharCheckmark" ,state.passwordValidation.eightChars);

    setPV({
      lowerCase: lowerCaseV,
      upperCase: upperCaseV,
      special: specialV,
      numerical: numericalV,
      eightChars: eightCharsV,
    });
  };


  const [redlineCheck, setRedLines] = useState({
    companyName: false,
    companyAddress: false,
    companyActivities: false,
    name: false,
    email: false,
    password: false
  });
  const createAccount = () => {
    // console.log(companyName);  
  
    // console.log(companyAddress);
    // console.log(companyActivities);
  
    // console.log(name);
    // console.log(email);
    // console.log(password);

    let redLineCompanyName = false;
    let redLineCompanyAddress= false;
    let redLineCompanyActivities= false;
    let redLineName= false;
    let redLineEmail= false;
    let redLinePassword= false;

    let passes = true;
    if(passwordValidation.lowerCase && passwordValidation.upperCase && passwordValidation.special && passwordValidation.numerical && passwordValidation.eightChars){

    } else {
      passes = false;
      redLinePassword=true;
    }

    if(!emailValidation){
      passes = false;
      redLineEmail = true;
    }

    if(companyName.length < 1){
      passes = false;
      redLineCompanyName = true;
    } 
    if(companyAddress.length < 1){
      passes = false;
      redLineCompanyAddress = true;
    }
    // if(companyActivities.length < 1){ //not compulsory
    //   passes = false;
    //   redLineCompanyActivities = true;
    // }
    if(name.length < 1){
      passes = false;
      redLineName = true;
    }

    setRedLines({
      companyName: redLineCompanyName,
      companyAddress: redLineCompanyAddress,
      companyActivities: redLineCompanyActivities,
      name: redLineName,
      email: redLineEmail,
      password: redLinePassword
    });


    if(passes){
      console.log("Passes");
      signup();
    } else {
      console.log("Fails");
    }
  }

  const [uploading, setUploading] = useState(false);
  const signup = async () => {
    setUploading(true);
    const profile = {
        name: name,
        email: email,
        password: password,
        address: "",
        membershipNumber: "",
        company: companyName,
        companyAddress: companyAddress,
        insuranceDateFrom: "",
        insuranceDateUntil: "",

        bisActivities: companyActivities,
        phone: "",
    }
    const signupPayload = {
      ...profile,
      // brokerId: "92136fcf-c034-4a67-9768-29c719830ff8", //Teru, Time Insurance
      // brokerId: "49ad5dfa-8ce1-4248-aba8-00e0b3ca8b0f", //Ben, Sentinel
      brokerId: "8c001226-531b-4f23-92e3-451dcc7c0637", //Craig Carlson, Carlson Risk
    };
     
    // await new Promise((resolve) => setTimeout(resolve, 2000));
    await fetch(`${process.env.NEXT_PUBLIC_SIGN_UP}`, {
      method: "POST",
      body: JSON.stringify(signupPayload),
      headers: {
        "Content-Type": "application/json",
      },
    }).then((res) => {
      setUploading(false);
      router.push({
        pathname: "/success",
      });
    }).catch((e) => {
      setUploading(false);
      // router.push({
      //   pathname: "/success",
      // });
    });
    setUploading(false);
  }

  // const checkEmail = (value) => {
  //   setEmail(value);
  // }



  return (
    <div className={styles.formCard}>
      {uploading && <Loader>Uploading Files</Loader>}
      <div className={styles.titleLayout}>
        <div>
          <p className={styles.Title}>Get Started with Folio</p>
          <p className={styles.subTitle}>{"Fill in the information below and we'll get in touch"}</p>
        </div>
        <div className={styles.brokerLogoImage}>
        </div>
      </div>


      <input
        type="text"
        className={styles.form__input2}
        id="companyInput"
        placeholder="Company Name"
        required=""
        onChange={(e) => changeCompanyName(e)}
      />

      <div className={styles.dropDown}>
          {companySearchResults.map((value, index) => {
            return (
              <p key={index} className={styles.companySearchOptions}
                onClick={() => setCompany(value)}
              >
                {value.name}
              </p>
            );
          })}
         
      </div>
      {redlineCheck.companyName && <p className={styles.redline}>Please enter your company{"'"}s name</p>}

      <input
        type="text"
        className={styles.form__input2}
        id="companyAddress"
        placeholder="Company Address"
        required=""
        onChange={(e) => setCompanyAddress(e.target.value)}
      />

    {redlineCheck.companyAddress && <p className={styles.redline}>Please enter your company{"'"}s address</p>}

    <textarea
          // rows="5"
          // cols="10"
          name="businessActivities"
          // ref={businessActivitiesRef}
          className={styles.form__textarea}
          id="businessActivities"
          placeholder="Business Activities"
          required=""
          type="text"
          onChange={(e) => setCompanyActivities(e.target.value)}

          // value={folioDetails.bisActivities}
          // onChange={(e) => editFolioDetails(e)}
        />

      <p className={styles.createAccount}>Create your Folio Account</p>
      <input
        type="text"
        className={styles.form__input2}
        id="name"
        placeholder="Contact Name"
        required=""
        onChange={(e) => setName(e.target.value)}
      />
    {redlineCheck.name && <p className={styles.redline}>Please enter your first and last name</p>}


       <input
        type="text"
        className={styles.form__input2}
        id="email"
        placeholder="Email"
        required=""
        onChange={(e) => checkEmail(e.target.value)}
      />
      {redlineCheck.email && <p className={styles.redline}>Please enter a valid email</p>}
       {emailValidation && (
              <p className={styles.passwordHas}>* Valid Email </p>
      )}
       {!emailValidation && (
              <p className={styles.passwordReq}>* Valid Email </p>
      )}
      <input
        type="password"
        className={styles.form__input2}
        id="password"
        placeholder="Password"
        required=""
        onChange={(e) => passwordCheck(e.target.value)}
      />
      {redlineCheck.password && <p className={styles.redline}>Please enter a valid password</p>}

       {passwordValidation.eightChars && (
              <p className={styles.passwordHas}>*Minimum has 8 Characters </p>
            )}
            {!passwordValidation.eightChars && (
              <p className={styles.passwordReq}>* Minimum has 8 Characters </p>
            )}
            {passwordValidation.lowerCase && (
              <p className={styles.passwordHas}>* Has Lowercase Letter</p>
            )}
            {!passwordValidation.lowerCase && (
              <p className={styles.passwordReq}>* Has Lowercase Letter</p>
            )}
            {passwordValidation.upperCase && (
              <p className={styles.passwordHas}>* Has Uppercase Letter</p>
            )}
            {!passwordValidation.upperCase && (
              <p className={styles.passwordReq}>* Has Uppercase Letter</p>
            )}
            {passwordValidation.numerical && (
              <p className={styles.passwordHas}>* Has a Number</p>
            )}
            {!passwordValidation.numerical && (
              <p className={styles.passwordReq}>* Has a Number</p>
            )}
            {passwordValidation.special && (
              <p className={styles.passwordHas}>* Has a Special Character</p>
            )}
            {!passwordValidation.special && (
              <p className={styles.passwordReq}>* Has a Special Character</p>
            )}

          <br></br>
          <div className={styles.submitGrid}>
              <p className={styles.finePrint}>{"By submitting this information you agree to Folio's"} <a className={styles.a} onClick={() => {window.open("https://www.folio.insure/termsconditions", "_blank");}}> Terms and Conditions </a> {"and for a trusted advisor to contact you."}</p>
              <button
              type="button"
              className={styles.createAccountButton}
              onClick={() => createAccount()}
            >
              Submit
            </button>
          </div>
    </div>
  )
}
